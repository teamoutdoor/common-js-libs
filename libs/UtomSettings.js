function UtomSettings(host, searchhost) {
	this.host = host;
	this.searchhost = searchhost;
}

function getSettings(){
    return {
        "utomapi": {
            "host": this.host,
                "guide": this.host+"/guide",
                "spot": this.host+"/spot",
                "stream": this.host+"/stream",
                "user": this.host+"/user",
                "follow": this.host+"/connection",
                "content": this.host+"/content",
                "segment": this.host+"/segment",
                "activity": this.host+"/activity",
                "multimedia": this.host+"/multimedia",
                "login": this.host+"/login",
                "schema": this.host+"/{type}/schema",
                "sports": this.host+"/sports"
        },
		"search" : {
			"domain": this.searchhost+"/route/search/querystring",
			"stream": this.searchhost+"/stream/search/querystring"
		}
    };
}

UtomSettings.prototype.getSettings = getSettings;
module.exports = UtomSettings;

   