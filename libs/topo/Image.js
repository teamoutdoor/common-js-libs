class Image{
    constructor(multimediaItem) {
        this.multimediaItem = multimediaItem;

    }

    getTopoImage() {
        return "http://res.cloudinary.com/outdoor-online/image/upload/"+ this.multimediaItem.url + ".JPEG";

    }

    getSizedTopoImage (width) {
        if(width > 5000){
            width = 5000;
        }
        return "http://res.cloudinary.com/outdoor-online/image/upload/w_" + width+"/"+ this.multimediaItem.url + ".JPEG";
    }


}

module.exports = Image;