

function adjustCoordinates(point, width, height) {
    var originalWidth = point.width;
    var originalHeight = point.height;
    point.x = point.x * (width / originalWidth);
    point.y = point.y * (height / originalHeight);

    return point;

}

module.exports = {
    adjustCoordinates: adjustCoordinates
};